
const express = require('express');
const app = express();


const authRouter = require("./routes/auth");
const homeRouter = require("./routes/main");
const mongoose = require('mongoose');
const dotenv = require("dotenv");
 


app.use(express.json());

app.use("/", homeRouter)
app.use("/api/user", authRouter);

dotenv.config();

// DB Connecting
mongoose.connect(
    process.env.MONGODB_CON, 
    {
        useNewUrlParser: true ,
        useUnifiedTopology: true 
    },
    () => {
        console.log("DB connecting ...");
    }
);




const port = process.env.PORT || 3000;
app.listen(port, () =>  console.log(`Server started on  port ${port}`));
