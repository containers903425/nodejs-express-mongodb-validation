const express = require('express');
const path = require('path');
const os = require('os');



const router = express.Router();
const platform = os.platform();
const dirname = path.dirname;
const dir = dirname(__dirname);


router.get('/', (req, res) => {
    res.sendFile(dir + '/index.html');
});
router.get('/about', (req, res) => {
    res.sendFile(dir + '/about.html');
});

module.exports = router;
