const router = require("express").Router();
const User = require('../model/user');
const { registerValidation, loginValidation } = require('../validation/validation-user');
const bcrypt = require('bcryptjs');




router.post('/register', async (req, res) => {
    console.log("register");

    const { error } = registerValidation(req.body);
    if(error)  return res.status(400).send(error.details[0].message);

    const emailExist = await User.findOne({email : req.body.email});
    console.log("cheking user exist.." + emailExist);
    if(emailExist)  res.status(400).send('Email already exists');

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password , salt);
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });
    console.log("name : " + user.name);
    console.log("email : " + user.email);

    try {
        console.log("waiting saveUser");
        const savedUser = await user.save();

        res.send({user : user._id});


        console.log("ended registers")
        // res.send('test'); 
    } catch (err) {
        res.status(400).send(err);
    }
});



router.post('/login', async (req, res) => {
        const { error } = loginValidation(req.body);
        if(error)  return res.status(400).send(error.details[0].message);

        const userExist = await User.findOne({email : req.body.email});
        if(!userExist) res.status(400).send('Email is not found');

        if (userExist.password){
            const comparePass = await bcrypt.compare(req.body.password, userExist.password);
            if(!comparePass) return res.status(400).send('Invalid password');
        }else{
            res.status(400).send('password is undefined');
        }

        res.send('Success Login!');
});

module.exports = router;
