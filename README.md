# Node.js Express API example authentication

- install express
```bash
npm install express
```


### joi validation module

- install joi
```bash
npm install joi --save
```


#### 1) validation 파일 생성  

- create validation folder and file `validation.js`
```
~/validation/validation.js
```  

in validation.js import joi module
```javascript   
const Joi = require('joi');
```


#### 2) Validate user input

- create a function to validate user input for validate user registration that parameters are name, email, password in a object data.
```javascript   
const registerValidation = (data) => {

    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema);

};
```
- Export the function

```javascript   
module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
```





### joi valdate data

#### 1) validation.js import
the file `validation.js` import to `auth.js` file in the folder `routes` 
```javascript   
const { registerValidation, loginValidation } = require('../validation/validation-user'); 
```   


#### 2) auth.js 수정하기 

- require express `router` for routing for the api `post` request and define path `/register` and `/login` for user registration and login.

```javascript   
router.post('/register', async (req, res) => {
    const { error } = registerValidation(req.body);
    if(error)  return res.status(400).send(error.details[0].message);
}
```  

- validate user input if `error` return `400` status code and error message

```javascript   
    if(error)  return res.status(400).send(error);
```  


### authentication 

- create a const `emailExist` to check if the email already exists in the database. If the email not exists, return a `400` status code and an error message.


```javascript   
    const emailExist = await User.findOne({email : req.body.email});
    if(emailExist) res.status(400).send('Email already exists');
```  







### bcryptjs module

- using bcryptjs module for password `hashing`

- install `bcryptjs`
```bash
npm install bcryptjs --save
```

- require `bcryptjs` module in the file `auth.js` 
```javascript 
const bcrypt = require('bcryptjs'); // 암호화 
```  

- define a `salt` for password hashing
- hash the password using `bcrypt.hash()` function

```javascript 
const salt = await bcrypt.genSalt(10);
const hashedPassword = await bcrypt.hash(req.body.password , salt);
```  

- create a new user object and bind the schema values to save in the mongo db.
```javascript 
const user = new User({ 
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword 
});
``` 
- A example of the user data in database
![password](./images/pwd_01.png)

### define a login api

#### 1) define a path for login api in the file `auth.js`

```javascript 
router.post('/login', async (req, res) => {

});
```  


#### 2) Validation data

- in the middleware function, validate the user input data for login. If the data is not valid return a `400` status code and an error message.

```javascript 
const { error } = loginValidation(req.body);
if(error)  return res.status(400).send(error.details[0].message);
```  
- the user's email must be the same as the email in the database
![password](./images/login_01.png)   


#### 3) Find user email in the database

- check if the user's email exists in the database. If the email does not exist, return a `400` status code and an error message.

```javascript 

const userExist = await User.findOne({email : req.body.email});
if(!userExist) res.status(400).send('Email is not found');

```  

#### 4) Compare password 

- compare the user's password with the password in the database. If the password is incorrect, return a `400` status code and an error message.
- for compare use `bcrypt.compare()` function in the `bcryptjs` module


```javascript 
const comparePass = await bcrypt.compare(req.body.password, userExist.password);
if(!comparePass) return res.status(400).send('Invalid password');
```  
- the user's password must be the same as the password in the database, this data is hashed or encrypted.
![password](./images/login_02.png)   


#### 5) Login success

- if the user's email and password are correct, return a `200` status code and a success message.

```javascript 
res.send('Success Login!'); 
```  
- example
![password](./images/login_03.png)

# this is a simple example of user registration and login using node.js express api with authentication.

